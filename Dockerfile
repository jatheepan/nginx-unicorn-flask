FROM python:3.6

RUN apt-get update && apt-get install python-pip nginx -y
RUN rm /etc/nginx/sites-enabled/default
RUN ln -s /etc/nginx/sites-available/flask_settings /etc/nginx/sites-enabled/flask_settings

RUN pip install Flask==1.0.2
RUN pip install gunicorn==19.9.0

COPY nginx_flask_settings /etc/nginx/sites-available/flask_settings
COPY app /app
WORKDIR /app

CMD service nginx start ; gunicorn main:app
