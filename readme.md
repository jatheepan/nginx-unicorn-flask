# Run Flask on nginx with unicorn

## Build docker image

```bash
git clone https://jatheepan@bitbucket.org/jatheepan/nginx-unicorn-flask.git
cd nginx-unicorn-flask
docker build -t nginxflask .
docker run --name flaskapp -p 80:80 nginxflask # where -p hostport:containerport
```

Open browser
http://localhost

## License
[MIT](https://choosealicense.com/licenses/mit/)
